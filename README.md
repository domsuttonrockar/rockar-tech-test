# Rockar Tech Test

### Objective
To demonstrate your OOP and Unit Testing skills

### Task
Create a working application with a GraphQL endpoint (schema and resolvers) at which we could query for Customer and Product details, but with a view that this could expand to contain any number of objects in the future. The response should be in valid JSON format.

The product.csv and customer.csv files provided contain dummy data in order to fulfil these requests, though within the application it is expected that data can be sourced from either a .csv file or DB connection (to be configurable via an .env file).

Whilst no external connection will be required, where appropriate stub out any external data connection.

Following coding standards, please add unit tests, docblocks, comments, etc…

Less is more, but sometimes a plugin will be more time efficient than writing something from scratch - Use your best judgement and document any necessary setup requirements in the README.

Submissions to be sent over as separate git repositories with suitable commits, messages, tags, etc…


### Assessment
Your submission will be assessed on your ability to present a practical understanding of the OOP Principles (Abstraction, Encapsulation, Polymorphism, Implementation), APIs, coding standards and unit testing.

Points will be deducted for any redundant code left over or for any code not fully refactored.
